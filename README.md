# Mobile Developer Portfolio - Jonathan Ixcayau

Welcome to my mobile developer portfolio! This project serves as a showcase of my skills, projects, and experience as a mobile developer.

## Table of Contents

- [Introduction](#introduction)
- [Projects](#projects)
- [Skills](#skills)
- [Technologies](#technologies)
- [Contact](#contact)

## Introduction

Hello! I'm Jonathan Ixcayau, a passionate mobile developer with a strong background in creating innovative and user-friendly applications. My goal is to leverage my skills and experience to deliver high-quality mobile solutions that meet the needs of users and clients.

## Skills

- Mobile App Development
- UI/UX Design
- Cross-Platform Development
- Responsive Design
- Testing and Debugging
- Problem Solving
- Collaboration and Teamwork

## Technologies

- **Programming Languages:** Swift, Swift UI, Kotlin, Jetpack Compose, Java
- **Frameworks:** Flutter, React Native
- **Tools:** Xcode, Android Studio, Visual Studio Code
- **Version Control:** Git, GitHub, Gitlab, Bitbucket
- **Database:** Firebase, SQLite, Mongo DB

## Contact

Feel free to reach out to me for collaboration, job opportunities, or just to say hello!

- **Portfolio:** [Portfolio Website](https://jonathan.ixcayau.com)
- **Email:** jonathan@ixcayau.com
- **LinkedIn:** [LinkedIn Profile](https://www.linkedin.com/in/jonathanixcayau/)
- **Gitlab:** [Gitlab Profile](https://gitlab.com/users/jonynija/projects)
- **Stackoverflow:** [Stackoverflow Profile](https://stackoverflow.com/users/11891580/jonathan-ixcayau)

Thank you for visiting my portfolio!

## Repo

Fork from github [project](https://github.com/midudev/porfolio.dev)

## Build Project

```bash
npm install
npm run build
cp dist/en/index.html dist/index.html
```

Then deploy with firebase hosting

```bash
firebase deploy --only hosting
```
