export const intl: LangMap = {
  en: {
    page: {
      title:
        "Jonathan Ixcayau - Senior Mobile Developer | Expert in Flutter, Java, Kotlin, Swift, Android, iOS | Innovator",
      desc: "Hire Jonathan Ixcayau to elevate your mobile app projects with over 5 years of proven expertise and a track record of delivering innovative solutions.",
    },
    navbar: {
      about: "About",
      experience: "Experience",
      projects: "Projects",
      stack: "Technologies",
    },
    about: {
      badge: "Available for Opportunities",
      partA: "Hello, I'm Jonathan",
      partB: "Senior Mobile Developer with a passion for innovation.",
      partC:
        "I bring over 5 years of experience in crafting exceptional mobile applications.",
      partD: "Based in Guatemala.",
      partE:
        "My focus lies in creating unique and revolutionary applications that leave a lasting impact.",
    },
    sections: {
      experience: "Experience",
      projects: "Projects",
      stack: "Technologies",
    },
    job: {
      list: [
        {
          date: "November 2024 - Present",
          title: "Senior Mobile App Developer",
          description:
            "Innovating digital healthcare at iDoctus through revolutionary mobile solutions.",
          company: "iDoctus",
        },
        {
          date: "November 2020 - January 2025",
          title: "Senior Mobile App Developer",
          description:
            "Led the development of Agriconecta, a revolutionary mobile app that transformed the agricultural sector at Popoyán, driving downloads and user satisfaction. Additionally, developed an administrative platform with Flutter and guided teams in critical decision-making.",
          company: "Popoyán",
        },
        {
          date: "December 2021 - August 2022",
          title: "Mobile Developer",
          description:
            "At Tribal Worldwide, created innovative solutions using Dart, Java, Swift, and Kotlin, prioritizing code quality and leading projects like a coffee shop app built with Jetpack Compose.",
          company: "Tribal Worldwide",
        },
        {
          date: "November 2019 - November 2020",
          title: "Android Developer",
          description:
            "Transformed banking apps at Digital Geko, implementing secure logins, enhancing performance, and integrating Huawei mobile services for exceptional user experiences.",
          company: "Digital Geko",
        },
        {
          date: "January 2019 - June 2020",
          title: "Full Stack Developer",
          description:
            "Developed 95% of a Flutter app at Inedito, optimizing searches with MongoDB and building an efficient backend with Node.js and Firebase.",
          company: "Inedito",
        },
      ],
      moreInfo: "More Info",
    },
    project: {
      projectA: {
        desc: "Elevated a Flutter project into a multi-platform masterpiece, seamlessly optimized for Android, iOS, macOS, and Web. Achieved flawless AdMob integration for uninterrupted monetization while leading migrations to newer versions, ensuring superior performance and efficiency.",
      },
      projectB: {
        desc: "Developed an innovative memory game app using SwiftUI, revolutionizing interactive entertainment. Featuring three difficulty levels and programming-themed cards, the app integrates a secure database for personalized experiences and results tracking, highlighting my commitment to engaging, state-of-the-art solutions.",
      },
      projectC: {
        desc: "Designed a groundbreaking Pokédex app using Jetpack Compose, redefining mobile experiences with Firebase authentication for seamless Google and Facebook login. Enhanced user retention through intuitive design and robust functionality, establishing it as a market leader.",
      },
      projectD: {
        desc: "Created a dynamic portfolio website with Astro and Firebase Hosting, showcasing a comprehensive collection of successful projects and contributions. This platform exemplifies my technical expertise, dedication to innovation, and engagement with the developer community.",
      },
      projectE: {
        desc: "Recognized as a key contributor on Stack Overflow, with over 700 reputation points. Consistently delivering expert insights and fostering collaboration within the mobile development community, demonstrating leadership and a commitment to knowledge sharing.",
      },
      imageAlt: "Screenshot of project",
    },
    stack: {
      appStores: "App Stores",
      multiplatform: "Multiplatform",
      mobile: "Mobile",
      web: "Web",
      db: "Databases",
      gsuite: "Google Suite",
      ai: "Artificial Intelligence",
      services: "Other Services",
      git: "Git",
      cicd: "CI/CD",
    },
    hobbies: {
      coffee: {
        title: "Coffee",
        desc: "I enjoy specialty coffee for its unique flavors, which are often more nuanced and complex than regular coffee. The attention to detail in sourcing, roasting, and brewing specialty beans results in a rich, aromatic cup that offers me a delightful sensory experience, making it a favorite for me as a coffee enthusiast.",
      },
      bike: {
        title: "Biking",
        desc: "I enjoy biking because it's a great way for me to stay active while exploring my surroundings. The freedom of riding, the sense of accomplishment from conquering challenging routes, and the opportunity to connect with nature or the cityscape make biking a thrilling and fulfilling experience for me.",
      },
    },
  },
  es: {
    page: {
      title:
        "Jonathan Ixcayau - Desarrollador Móvil Senior | Experto en Flutter, Java, Kotlin, Swift, Android, iOS | Innovador",
      desc: "Contrata a Jonathan Ixcayau para elevar tus proyectos de aplicaciones móviles con más de 5 años de experiencia probada y un historial de soluciones innovadoras.",
    },
    navbar: {
      about: "Sobre mí",
      experience: "Experiencia",
      projects: "Proyectos",
      stack: "Tecnologías",
    },
    about: {
      badge: "Disponible para Oportunidades",
      partA: "Hola, soy Jonathan",
      partB: "Desarrollador Móvil Senior con pasión por la innovación.",
      partC:
        "Aporto más de 5 años de experiencia en la creación de aplicaciones móviles excepcionales.",
      partD: "De Guatemala.",
      partE:
        "Mi enfoque se centra en crear aplicaciones únicas y revolucionarias que dejen un impacto duradero.",
    },
    sections: {
      experience: "Experiencia",
      projects: "Proyectos",
      stack: "Tecnologías",
    },
    job: {
      list: [
        {
          date: "Noviembre 2024 - Presente",
          title: "Desarrollador Sénior de Aplicaciones Móviles",
          description:
            "Innovando la salud digital en iDoctus mediante soluciones móviles revolucionarias.",
          company: "iDoctus",
        },
        {
          date: "Noviembre 2020 - Enero 2025",
          title: "Desarrollador Sénior de Aplicaciones Móviles",
          description:
            "Lideré el desarrollo de Agriconecta, una app móvil revolucionaria que transformó el sector agrícola en Popoyán, impulsando descargas y satisfacción del usuario. Además, creé una plataforma administrativa con Flutter y orienté equipos en decisiones críticas.",
          company: "Popoyán",
        },
        {
          date: "Diciembre 2021 - Agosto 2022",
          title: "Desarrollador Móvil",
          description:
            "En Tribal Worldwide, creé soluciones innovadoras con Dart, Java, Swift y Kotlin, priorizando calidad de código y liderando proyectos como una app de cafetería con Jetpack Compose.",
          company: "Tribal Worldwide",
        },
        {
          date: "Noviembre 2019 - Noviembre 2020",
          title: "Desarrollador Android",
          description:
            "Transformé aplicaciones bancarias en Digital Geko, con logins seguros y mejoras en rendimiento, además de integrar servicios móviles de Huawei para experiencias excepcionales.",
          company: "Digital Geko",
        },
        {
          date: "Enero 2019 - Junio 2020",
          title: "Desarrollador Full Stack",
          description:
            "Desarrollé el 95% de una app Flutter en Inedito, optimizando búsquedas con MongoDB y creando un backend eficiente con Node.js y Firebase.",
          company: "Inedito",
        },
      ],
      moreInfo: "Más Información",
    },
    project: {
      projectA: {
        desc: "Elevé un proyecto de Flutter a una obra maestra multiplataforma, optimizada sin esfuerzo para Android, iOS, macOS y Web. Logré una integración impecable de AdMob para una monetización continua mientras lideraba migraciones a versiones más nuevas, garantizando un rendimiento y eficiencia superiores.",
      },
      projectB: {
        desc: "Desarrollé una innovadora app de memoria usando SwiftUI, revolucionando el entretenimiento interactivo. Con tres niveles de dificultad y cartas con temática de programación, la app integra una base de datos segura para experiencias personalizadas y seguimiento de resultados, destacando mi compromiso con soluciones atractivas y de vanguardia.",
      },
      projectC: {
        desc: "Diseñé una innovadora app Pokédex usando Jetpack Compose, redefiniendo las experiencias móviles con autenticación de Firebase para un inicio de sesión fluido con Google y Facebook. Mejoré la retención de usuarios mediante un diseño intuitivo y una funcionalidad robusta, posicionándola como líder del mercado.",
      },
      projectD: {
        desc: "Creé un sitio web dinámico de portafolio con Astro y Firebase Hosting, mostrando una colección integral de proyectos exitosos y contribuciones destacadas. Esta plataforma ejemplifica mi experiencia técnica, dedicación a la innovación y compromiso con la comunidad de desarrolladores.",
      },
      projectE: {
        desc: "Reconocido como un colaborador clave en Stack Overflow, con más de 700 puntos de reputación. Ofreciendo constantemente conocimientos expertos y fomentando la colaboración en la comunidad de desarrollo móvil, demostrando liderazgo y compromiso con el intercambio de conocimientos.",
      },
      imageAlt: "Captura de pantalla del proyecto",
    },
    stack: {
      appStores: "Tiendas de Aplicaciones",
      multiplatform: "Multiplataforma",
      mobile: "Móvil",
      web: "Web",
      db: "Bases de Datos",
      gsuite: "Suite de Google",
      ai: "Inteligencia Artificial",
      services: "Otros Servicios",
      git: "Git",
      cicd: "CI/CD",
    },
    hobbies: {
      coffee: {
        title: "Café",
        desc: "Disfruto del café especial por sus sabores únicos, que suelen ser más matizados y complejos que el café regular. La atención al detalle en la obtención, tostado y preparación de los granos especiales resulta en una taza rica y aromática que me ofrece una experiencia sensorial encantadora, convirtiéndolo en uno de mis favoritos como entusiasta del café.",
      },
      bike: {
        title: "Ciclismo",
        desc: "Disfruto del ciclismo porque es una excelente manera de mantenerme activo mientras exploro mi entorno. La libertad de andar en bicicleta, la sensación de logro al conquistar rutas desafiantes y la oportunidad de conectarme con la naturaleza o el paisaje urbano hacen que el ciclismo sea una experiencia emocionante y gratificante para mí.",
      },
    },
  },
} as const;

type LangMap = {
  [key: string]: any;
};
