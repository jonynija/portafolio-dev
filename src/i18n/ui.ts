export const languages: LangType = {
    en: 'English',
    es: 'Español',
};

export const defaultLang: string = 'en';
export const showDefaultLang = false;

type LangType = {
    [key: string]: string;
};
