import { intl } from "./lang";
import { defaultLang } from "./ui";

export function getLangFromUrl(url: URL): string {
  const [, lang] = url.pathname.split("/");
  if (lang in intl) return (lang as keyof typeof intl).toString();

  return defaultLang;
}

export function useTranslations(lang: keyof typeof intl) {
  return function t(key: string) {
    const keys = key.split(/\.|\[|\]\.?/).filter(Boolean); // Split by dots, brackets, and remove empty parts
    let value = intl[lang];

    for (const it of keys) {
      if (value === undefined || value === null) {
        return ""; // Return empty string if a key is not found
      }
      value = value[it];
    }

    return typeof value === "string" ? value : "";
  };
}
